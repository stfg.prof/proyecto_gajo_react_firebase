# Proyecto_Gajo_React_Firebase

## sitio en construccion

[mapa](https://gitlab.com/stfg.prof/proyecto_gajo_react_firebase/-/raw/main/imgs/mapa.jpg)

## TO DO:

- [ ] README

  - [ ] agregar descripcion detalladda del proyecto
  - [ ] especificar tecnologias usadas
  - [ ] explicar el como estan constituidas la funcionalidades
  - [ ] agregar README a los submodulos

- [ ] appReact

  - [ ] desacoplar el metodo de carga de archivo y el de imagen
  - [ ] generar una preview de los datos cargados anteriormente
  - [ ] generar rutas diferentes segun si ya se han cargado datos o aun no
  - [ ] cambiar el nombre de las imgs al subirlas
  - [ ] disponer un modal al tocar cada punto
  - [ ] crear vista de about
  - [ ] agregar icono
  - [ ] agregar metadata

- [ ] generar css

  - [ ] clases segun ruta
  - [ ] estilo de btns
  - [ ] estilo de formulario general
  - [ ] estilo de login
  - [ ] estilo del registro
  - [ ] sacar el scroll solo en el mapa del index
  - [ ] cambiar imgs de los geoPoints

- [ ] firebase
  - [ ] borrar imagenes ante cambios de formulario
  - [ ] deploy de un demo de la app (dejar solo login desde google?)
  - [ ] generar rules de seguridad para firestore y riebase
  - [ ] redefinir nombre de la variable que contine la lista de puntos (geoloc)
